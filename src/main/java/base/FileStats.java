package base;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Map;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.*;

@Builder
@Getter
@RequiredArgsConstructor
@JsonInclude(NON_NULL)
public class FileStats {

    private static final ObjectWriter PRETTY_PRINTER = new ObjectMapper().writerWithDefaultPrettyPrinter();

    private final String filename;
    private final int charsCount;
    private final Map<String, Long> wordsCounts;
    private final Map<String, List<String>> wordsSentences;
    private final long timeSpent;

    @Override
    public String toString() {
        try {
            return PRETTY_PRINTER.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return "{ \"serializationError\": \"" + e.getMessage() + "\" }";
        }
//        return "FileStats{\n" +
//                "\tfilename='" + filename + "\',\n" +
//                "\tcharsCount=" + charsCount + ",\n" +
//                "\twordsCounts=" + wordsCounts + ",\n" +
//                "\twordsSentences=" + wordsSentences + ",\n" +
//                "\ttimeSpent=" + timeSpent +
//                "}\n";
    }
}
