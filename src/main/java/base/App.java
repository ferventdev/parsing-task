package base;

import picocli.CommandLine;

public class App {

    public static void main(String[] args) {
//        args = "   word1,word2,word3 -c file1 -v file2 -e    file3  file4  ".trim().split("\\s+");
        CommandLine.run(new Args(), args);
    }
}
