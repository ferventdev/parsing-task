package base;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class Args implements Runnable {

    @Parameters(index = "0", arity = "1", split = ",", paramLabel = "WORD", description = "words to be sought")
    private List<String> words;

    @Parameters(index = "1..*", arity = "1..*", paramLabel = "FILE", description = "files where to seek the words")
    private List<String> filenames;

    @Option(names = "-w", description = "count number of provided word(s) occurrences in each file")
    private boolean wordsCounter = false;

    @Option(names = "-c", description = "count number of characters in each file")
    private boolean charactersCounter = false;

    @Option(names = "-v", description = "output verbosity flag, if on then the output should contains information about time spend on data scraping and data processing")
    private boolean verbosity = false;

    @Option(names = "-e", description = "extract sentences which contain given words")
    private boolean extraction = false;

    @Override
    public void run() {
//        System.out.println("words: " + words);
//        System.out.println("filenames: " + filenames);
//        System.out.println("wordsCounter: " + wordsCounter);
//        System.out.println("charactersCounter: " + charactersCounter);
//        System.out.println("verbosity: " + verbosity);
//        System.out.println("extraction: " + extraction + '\n');

        int processorsCount = Runtime.getRuntime().availableProcessors();
        ExecutorService threadPool = Executors.newFixedThreadPool(Math.min(filenames.size(), processorsCount * 2));

        List<CompletableFuture<FileStats>> futures =
                filenames.stream()
                         .map(filename ->
                                 CompletableFuture
                                         .supplyAsync(() -> new ScrapTask(filename, words).call(), threadPool)
                                         .exceptionally(ex -> {
                                             System.out.println(ex.getMessage());
                                             return null;
                                         }))
                         .collect(Collectors.toList());

        futures.stream()
               .map(CompletableFuture::join)
               .filter(Objects::nonNull)
               .forEach(System.out::println);

        closePool(threadPool);
    }

    private void closePool(ExecutorService threadPool) {
        threadPool.shutdown();
        try {
            if (!threadPool.awaitTermination(10, TimeUnit.SECONDS)) threadPool.shutdownNow();
        } catch (InterruptedException e) {
            threadPool.shutdownNow();
        }
    }

    @RequiredArgsConstructor
    static class ScrapTask implements Callable<FileStats> {

        private static final Pattern SENTENCE_PATTERN =
                Pattern.compile("[\"']?[A-Z][^.?!]+((?![.?!]['\"]?\\s[\"']?[A-Z][^.?!]).)+[.?!'\"]+", Pattern.MULTILINE);
//        private static final Pattern SENTENCE_DELIMITER = Pattern.compile("\\s*[.?!]\\s+");
        private static final Pattern WORD_DELIMITER = Pattern.compile("\\s*[\\s,;(){}<>\\[\\]]+\\s*");
        private final String filename;
        private final List<String> words;

        @Override
        public FileStats call() {
            Instant startingTime = Instant.now();

            String text;
            try (BufferedReader reader = Files.newBufferedReader(Paths.get(filename), StandardCharsets.UTF_8)) {
                text = IOUtils.toString(reader);
//                String[] sentences = SENTENCE_DELIMITER.split(text);
                Matcher sentenceMatcher = SENTENCE_PATTERN.matcher(text);

                Map<String, Long> wordsCount = words.stream()
                                                       .collect(toMap(Function.identity(), word -> 0L));
                Map<String, List<String>> wordsSentences = words.stream()
                                                                .collect(toMap(Function.identity(), word -> new ArrayList<>()));

                while(sentenceMatcher.find()) {
                    String sentenceWithDot = sentenceMatcher.group();
                    String sentence = (sentenceWithDot.endsWith(".")) ?
                            sentenceWithDot.substring(0, sentenceWithDot.length() - 1) :
                            sentenceWithDot;
                    String[] tokens = WORD_DELIMITER.split(sentence);
//                    System.out.println("Sentence: " + sentence);
//                          System.out.print("Its tokens: ");
//                          for (String token : tokens) {
//                              System.out.print(token + " ");
//                          }
//                          System.out.println();

                    words.forEach(word -> {
                        long countOccurences = Arrays.stream(tokens)
                                                     .filter(token -> StringUtils.equalsIgnoreCase(token, word))
                                                     .count();
                        if (countOccurences > 0) {
                            wordsSentences.compute(word, (w, list) -> {
                                list.add(sentenceWithDot);
                                return list;
                            });
                            wordsCount.compute(word, (w, count) -> count + countOccurences);
                        }
                    });
                }

                Instant finishTime = Instant.now();

                return FileStats.builder()
                                .filename(filename)
                                .charsCount(text.codePointCount(0, text.length()))
                                .wordsCounts(Collections.unmodifiableMap(wordsCount))
                                .wordsSentences(Collections.unmodifiableMap(wordsSentences))
                                .timeSpent(Duration.between(startingTime, finishTime).toMillis())
                                .build();

            } catch (IOException e) {
                System.out.println(e.getMessage());
                return null;
            }
        }
    }
}
